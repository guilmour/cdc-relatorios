## To Deploy

First, install dependencies.
```
pip3 install --user -r requirements.txt
```

Then, run the Flask app:
```
flask run
```
