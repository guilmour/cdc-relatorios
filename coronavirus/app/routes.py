from io import StringIO, BytesIO
from flask import render_template, flash, redirect, send_file
from app import app
from app.forms import CompareForm
from app.compare_countries import get_similar_countries, get_fig_compare_rates
# from math import abs

# @app.route('/')
# @app.route('/index')
# def index():
#     user = {'username': app.config['SECRET_KEY']}
#     posts = [
#         {
#             'author': {'username': 'John'},
#             'body': 'Beautiful day in Portland!'
#         },
#         {
#             'author': {'username': 'Susan'},
#             'body': 'The Avengers movie was so cool!'
#         }
#     ]
#     form = CompareForm()
#     return render_template('index.html', title='Chose XXX', form=form)

@app.route('/')
@app.route('/index', methods=['GET', 'POST'])
def index():
    form = CompareForm()
    if form.validate_on_submit():
        flash('Contry requested: {}'.format(
            form.countries.data))
        simcountries = get_similar_countries(form.countries.data)
        return render_template('compare_init.html', country=form.countries.data, simcountries=simcountries) #redirect('/index')
    return render_template('index.html', form=form)

@app.route('/comparison/<country>/<country2>')
def comparison(country, country2):
    return render_template("compare_countries.html", country=country, country2 = country2)

@app.route('/figcomparerates/<country>/<country2>')
def figcomparerates(country, country2):
    fig = get_fig_compare_rates(country, country2)
    img = BytesIO()
    fig.savefig(img)
    img.seek(0)
    return send_file(img, mimetype='image/png')