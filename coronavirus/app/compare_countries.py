import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import importlib
import numpy as np
import seaborn as sns; sns.set()
import os
from scipy.spatial.distance import squareform
from scipy.spatial.distance import pdist, euclidean
from datetime import datetime


data_dir = '../data/coronavirus/'

def get_all_countries():
    df_countries = pd.read_csv(data_dir + 'all_countries.csv')
    return list(df_countries['Country'])

def get_df_similar_countries(country):
    df_orig = pd.read_csv(data_dir + 'total_cases_normalized.csv')
    df_orig_piv_day = df_orig.pivot(index='Country', columns='Day', values='TotalDeaths')

    df_orig_piv_day = df_orig_piv_day.fillna(0)

    sr_country = df_orig_piv_day.loc[country,]

    country_start = (sr_country > 0).idxmax()

    country_start_cases = (df_orig.set_index('Country').loc[country,].set_index('Day')['Total'] > 0).idxmax()

    df_countries_ahead = df_orig_piv_day[df_orig_piv_day.iloc[:, country_start - 14] > 0.0]

    df_countries_rate_norm = df_orig_piv_day.loc[df_countries_ahead.index, :]

    # df_countries_rate_norm = df_orig_piv_day.loc[['France', 'Italy'], :]

    df_countries_rate_norm = df_countries_rate_norm.append(df_orig_piv_day.loc[country,])

    # reverse order to keep base country on top
    df_countries_rate_norm = df_countries_rate_norm.iloc[::-1]

    sr_country = df_orig_piv_day.loc[country,]

    country_start = (sr_country > 0).idxmax()

    sr_country_compare = sr_country.loc[country_start:].dropna()

    sr_country = df_orig_piv_day.loc[country,]

    country_start = (sr_country > 0).idxmax()

    sr_country_compare = sr_country.loc[country_start:].dropna()

    df_countries_gap = pd.DataFrame({'Country': [], 'gap': [], 'dist': []})

    df_countries_gap = df_countries_gap.append(pd.Series([country, 0.0, -1], index=df_countries_gap.columns),
                                               ignore_index=True)

    for other_country in df_countries_rate_norm.index[1:]:
        sr_other_country = df_countries_rate_norm.loc[other_country,].fillna(0)

        min_dist = np.inf

        min_pos = 0

        for i in range(0, 1 + len(sr_other_country) - len(sr_country_compare)):
            sr_other_country_compare = sr_other_country[i: i + len(sr_country_compare)]
            dist = euclidean(sr_country_compare, sr_other_country_compare)
            if (dist < min_dist):
                min_dist = dist
                min_pos = i

        df_countries_gap = df_countries_gap.append(
            pd.Series([other_country, min_pos - country_start, min_dist], index=df_countries_gap.columns),
            ignore_index=True)

    df_countries_gap = df_countries_gap.set_index('Country')

    similar_countries = df_countries_gap.sort_values('dist')

    similar_countries['Similarity'] = similar_countries['dist'].apply(lambda x: 1.0 / np.sqrt(x + 1) if x >= 0 else 1)

    return similar_countries


def get_similar_countries(country):

    similar_countries = get_df_similar_countries(country)

    tuples = [tuple(x) for x in similar_countries[1:8].reset_index().to_numpy()]

    return tuples


def get_fig_compare_rates(country, country2):
    # Data for plotting
    t = np.arange(0.0, 2.0, 0.01)
    s = 1 + np.sin(2 * np.pi * t)

    fig, ax = plt.subplots()
    ax.plot(t, s)

    ax.set(xlabel='time (s)', ylabel='voltage (mV)',
           title='{} X {}'.format(country, country2))
    ax.grid()
    #
    # fig.savefig("test.png")
    # plt.show()
    return fig