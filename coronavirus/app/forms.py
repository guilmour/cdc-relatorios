from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, BooleanField, SubmitField
from wtforms.validators import DataRequired
from app.compare_countries import get_all_countries

countries_init = get_all_countries()

class CompareForm(FlaskForm):
    # numCountries = StringField('Num COuntries', validators=[DataRequired()])
    countries = SelectField(u'Country', choices=[(c, c) for c in countries_init], validators=[DataRequired()])
    submit = SubmitField('Show Report')